require 'open-uri'
require 'nokogiri'

out = File.open("artists.txt", 'w')
url = "http://songmeanings.com/artist/directory/"
for letter in 'a'..'z'
  puts "Crawling letter \"#{letter}\""
  for page in 1..400
    puts " -> page #{page}"
    html = nil
    begin
      `sleep 2`
      html = open("#{url}#{letter}/?page=#{page}", "User-Agent" => "Googlebot").read
    rescue
      puts " -> done crawling #{letter}"
      break
    end
    noko = Nokogiri::HTML(html)
    puts " -> getting anchors"
    tbody = noko.css("tbody")
    for ref in tbody.css("a")
      out << ref.to_s << "\n"
    end
  end
end

