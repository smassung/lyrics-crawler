require 'nokogiri'
require 'open-uri'
require 'json'

i = 1
lines = File.open("artists.txt", 'r').readlines
for line in lines
  m = line.match(/href="([^"]*)" title="([^"]*)"/)
  url = m.captures[0].gsub(/\/songs\//, "/discography/")
  artist = m.captures[1]
  puts "#{i}/#{lines.size}\t#{artist}"
  puts " -> #{url}"
  noko = Nokogiri::HTML(open(url).read)
  json = {"artist" => artist, "url" => url, "albums" => []}
  for div in noko.css("div.text")
    album = {}
    for header in div.css("h3")
      alb = header.css("a")
      link = alb.to_s.match(/href="([^"]*)"/).captures[0]
      album["title"] = alb.text
      album["url"] = link
    end
    album["date"] = nil
    for litem in div.css("li")
      begin
        album["date"] = Date.parse(litem.text)
      rescue
        next
      end
      puts " -> found album release date"
      break # found a date that could be parsed
    end
    json["albums"] << album
  end
  unless json["albums"].empty?
    puts " -> found albums"
    artist_filename = artist.gsub(/[^a-zA-Z0-9]/, "_")
    out = File.open("artists/#{i}_#{artist_filename}.json", 'w')
    out << JSON.pretty_generate(json)
    out.close()
  end
  `sleep 4`
  i += 1
end
