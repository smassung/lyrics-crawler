require 'open-uri'
require 'fileutils'
require 'json'
require 'nokogiri'

def get_text(url)
  begin
    text = open("http://songmeanings.com#{url}").read
    return text
  rescue
    puts "ERROR: waiting 10 minutes..."
    `sleep #{60 * 10}`
    return nil
  end
end

i = 1
lines = File.open("albums.txt", 'r').readlines
for f in lines
  f.chomp!
  puts "#{(i.to_f/49689*100).round(2)}%\t#{f}\t(#{i})"
  FileUtils.cp("artists/#{f}", "artists/#{f}.old")
  jdata = JSON.parse(File.open("artists/#{f}.old", 'r').read)
  for album in jdata["albums"]
    content = get_text(album["url"])
    while content.nil?
      content = get_text(url)
    end
    noko = Nokogiri::HTML(content)
    tbody = noko.css("tbody")
    album["tracks"] = []
    for anch in tbody.css("a")
      title = anch.text()
      url = anch.to_s.match(/http:\/\/songmeanings.com([^"]*)"/).captures[0]
      album["tracks"] << {"title" => title, "url" => url}
    end
    `sleep 4`
  end
  out = File.open("artists/#{f}", 'w')
  out << JSON.pretty_generate(jdata)
  i += 1
end
