require 'open-uri'

agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3"
base_url = "http://www.azlyrics.com/"
for url in File.open("artist-urls.txt", 'r')
  artist = url.split(" ")[0].gsub(/[^ A-Za-z0-9]/, "").downcase
  url = base_url + url.split(" ")[1]
  puts "Crawling #{url} (#{artist})..."
  `mkdir lyrics/#{artist}`
  out = File.open("lyrics/#{artist}/song-urls.txt", 'w')
  out << open(url, "User-Agent" => agent).read
  `sleep 10`
end
