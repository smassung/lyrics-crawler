require 'open-uri'
require 'json'
require 'fileutils'

def download(url)
  output = nil
  begin
    dir = File.dirname(url)
    FileUtils.mkdir_p("lyrics/#{dir}") unless File.directory?("lyrics/#{dir}")
    output = File.open("lyrics/#{url}", 'w')
  rescue
    puts "ERROR couldn't create file"
    exit
  end
  begin
    output << open("http://www.azlyrics.com/lyrics/#{url}").read
  rescue
    puts "ERROR couldn't open URL"
    exit
  end
end

def crawl(start_idx)
  idx = 0
  artist_list = JSON.parse(File.open("music.json", 'r').read)
  for artist in artist_list
    for album in artist["albums"]
      for track in album["tracks"]
        idx += 1
        next if idx < start_idx
        download(track["url"])
        puts "Downloaded song #{idx}"
        puts "  Artist: #{artist["name"]} Title: #{track["name"]}"
        `sleep 10`
      end
    end
  end
end

def main()
  start_idx = 0
  start_idx = ARGV[0].to_i if ARGV.size == 1
  crawl(start_idx)
end

main()
