require 'json'
require 'nokogiri'

def get_albums(text)
  page = Nokogiri::HTML(text)
  albums = []
  for album in page.css("div.album")
    next if album.text.match(/other songs:/) # skip songs with no album
    mdata = album.text.match(/"([^"]*)"/)
    name = "Unknown Album Name"
    name = mdata[1] unless mdata == nil
    year = 0
    mdata = album.text.match(/\(([^\)]*)\)/)
    year = mdata[1].to_i unless mdata == nil
    alb = {"name" => name, "year" => year, "tracks" => []}
    while album.next_element != nil
      album = album.next_element
      break if album.name == "div" # stop traversing if we go into next album
      next if album.name != "a" or album.text.empty?
      url = nil
      mdata = album.to_s.match(/href="([^"]*)"/)
      url = mdata[1][10..-1] unless mdata == nil # remove "../lyrics" from url
      name = album.text
      alb["tracks"] << {"name" => name, "url" => url}
    end
    albums << alb
  end
  return albums
end

def main()
  artists = []
  output = File.open("music.json", 'w')
  for dir in `ls lyrics/`.split(" ")
    text = File.open("lyrics/#{dir}/song-urls.txt", 'r').read
    mdata = text.match(/<title>(.*) lyrics<\/title>/)
    artist_name = "Unknown Artist"
    artist_name = mdata.captures[0] unless mdata == nil
    artist_data = {"name" => artist_name, "albums" => get_albums(text)}
    artists << artist_data
  end
  output << JSON.pretty_generate(artists)
end

main()
