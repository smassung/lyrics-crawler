require 'open-uri'

base_url = "http://www.azlyrics.com/"
output = File.open("artist-urls.txt", 'w')
for ch in ("a".."z").to_a
  puts "Crawling #{ch} artists..."
  index = open("#{base_url}#{ch}.html").read
  for m in index.scan(/<a href="([^"]*)">([^<]*)<\/a><br \/>/)
    url = m[0]
    artist = m[1].gsub(/[ \/\\]/, "_")
    output << "#{artist} #{url}\n"
  end
  `sleep 2`
end
